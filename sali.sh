#!/bin/bash

error() { clear ; echo -e "ERROR: $1" ; exit; }

# Check root permission
[ $EUID -ne 0 ] && error "Permission denied!\nRun this script as root"

clear

# Get branch
while getopts ":b:h" o; do
    case "${o}" in
        b)
            branch="${OPTARG}" ;;
        h)
	        printf "Optional arguments for custom use:\\n  -b: Git branch\\n  -h: Show this message\\n" && exit ;;
        *)
            printf "Invalid option: -%s\\n" "$OPTARG" && exit ;;
    esac
done
[ -z $branch ] && branch="master"

# Select and sort mirrors
pacman -Sy reflector fzf dialog --noconfirm || error "Can not install reflector, fzf and dialog"

read -rd '' countries <<'EOF'
Done
Australia
Austria
Bangladesh
Belarus
Belgium
Bosnia and Herzegovina
Brazil
Bulgaria
Canada
Chile
China
Colombia
Croatia
Czechia
Denmark
Ecuador
Finland
France
Georgia
Germany
Greece
Hong Kong
Hungary
Iceland
India
Indonesia
Iran
Ireland
Israel
Italy
Japan
Kazakhstan
Kenya
Latvia
Lithuania
Luxembourg
Netherlands
New Caledonia
New Zealand
North Macedonia
Norway
Paraguay
Philippines
Poland
Portugal
Romania
Russia
Serbia
Singapore
Slovakia
Slovenia
South Africa
South Korea
Spain
Sweden
Switzerland
Taiwan
Thailand
Turkey
Ukraine
United Kingdom
United States
Vietnam
EOF

while true
do
    country="$(printf '%s\n' "$countries" | fzf --layout=reverse --height 60% --color=16 --border --header "Select mirrors (select Done or press Esc when you're done):")"
    [ "$country" != "Done" ] && ! [ -z "$country" ] && chosencountries+="-c '$country' " || break
    countries="$(printf '%s\n' "$countries" | sed "/$country/d")"
done

reflcmd="reflector "$chosencountries" -a 12 --sort rate --save /etc/pacman.d/mirrorlist"
[ -z "$chosencountries" ] || { dialog --title "SALI Installation" --infobox "\nSorting mirrors..." 6 30 ; echo $reflcmd | bash ; }
clear

# Warings
dialog --defaultno --title "SALI Installation" --yesno "Welcome to SALI - Syaoran's Arch Linux Installer.\n\nOnly run this script if you know what you're doing or what this script does.\n\nSelect <No> to exit."  12 60 || error "User exits."

dialog --defaultno --title "SALI Installation" --yesno "Last warning!\n\nThis script will FORMAT your hard drive and install Arch linux. That means ALL YOUR DATA will be DELETED. Make sure to back up your data if you use this script.\n\nTo stop this script, select <No>."  12 60 || error "User exits."

# Get hostname
dialog --title "SALI Installation" --no-cancel --inputbox "Enter a name for your computer:" 10 60 "arch-pc" 2> hostname

# Get root user's password
pass1=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Enter a password for root user." 10 60 3>&1 1>&2 2>&3 3>&1);
pass2=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1);
while ! [ "$pass1" = "$pass2" ]; do
    unset pass2
    pass1=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Passwords do not match.\\n\\nEnter password again." 10 60 3>&1 1>&2 2>&3 3>&1)
    pass2=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1)
done
echo "$pass1" > rootpw.sali
unset pass1 pass2

# Get user's name and password
username=$(dialog --title "SALI Installation" --no-cancel --inputbox "Enter a name for the user account." 10 60 3>&1 1>&2 2>&3 3>&1)
while ! echo "$username" | grep "^[a-z_][a-z0-9_-]*$" >/dev/null 2>&1; do
    username=$(dialog --title "SALI Installation" --no-cancel --inputbox "Username not valid. Give a username beginning with a letter, with only lowercase letters, - or _" 10 60 3>&1 1>&2 2>&3 3>&1)
done
echo "$username" > username.sali
pass1=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Enter a password for $username." 10 60 3>&1 1>&2 2>&3 3>&1);
pass2=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1);
while ! [ "$pass1" = "$pass2" ]; do
    unset pass2
    pass1=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Passwords do not match.\\n\\nEnter password again." 10 60 3>&1 1>&2 2>&3 3>&1)
    pass2=$(dialog --title "SALI Installation" --no-cancel --insecure --passwordbox "Retype password." 10 60 3>&1 1>&2 2>&3 3>&1)
done
echo "$pass1" > userpw.sali
unset pass1 pass2

# Timezone
gettz() {
    clear
    region="$(ls /usr/share/zoneinfo/ | fzf --layout=reverse --height 60% --color=16 --border --header "Select your region (use arrow keys to move):")"
    city="$(ls /usr/share/zoneinfo/"$region" | fzf --layout=reverse --height 60% --color=16 --border --header "Select your city (use arrow keys to move):")"
    echo "$region/$city"
}

dialog --title "SALI Installation" --yesno "Do you want use the default time zone (Asia/Ho_Chi_Minh)?\n\nSelect <No> to get your own time zone."  8 65 && echo "Asia/Ho_Chi_Minh" > timezone.sali || gettz > timezone.sali

# Select filesystem
fs="$(dialog --no-cancel --title "SALI Installation" --menu "Select filesystem:" 0 0 0 1 "ext4 (default)" 2 "btrfs" 3>&1 1>&2 2>&3 3>&1)"

# Get swap and root partition size
SIZE[0]=$(dialog --title "SALI Installation" --no-cancel --inputbox "Enter swap partition size in GiB." 10 60 "4" 3>&1 1>&2 2>&3 3>&1)
[ "$fs" = "1" ] && SIZE[1]=$(dialog --title "SALI Installation" --no-cancel --inputbox "Enter root partition size in GiB." 10 60 "30" 3>&1 1>&2 2>&3 3>&1)

# re='^[0-9]+$'
# if ! [ ${#SIZE[@]} -eq 2 ] || ! [[ ${SIZE[0]} =~ $re ]] || ! [[ ${SIZE[1]} =~ $re ]] ; then
#     SIZE=(4 30);
# fi

# Get hard drive name
hd="$(lsblk -dpnlo NAME,SIZE -e 7,11 | fzf --layout=reverse --height 60% --color=16 --border --header "Choose hard drive:" | cut -d' ' -f1)"
echo $hd > hd.sali

# Get kernel
ker_choice="$(dialog --no-cancel --title "SALI Installation" --menu "Select kernel which you want to install:" 0 0 0 1 "linux (unstable)" 2 "linux-lts (stable)" 3>&1 1>&2 2>&3 3>&1)"

# Ensure the system clock is accurate
timedatectl set-ntp true

# Installation choices
[ -d /sys/firmware/efi ] && def_option="1" || def_option="2"
fdisk -l | grep Microsoft >/dev/null 2>&1 && def_option="3"
echo $def_option > option.sali

choice="$(dialog --no-cancel --title "SALI Installation" --default-item "$def_option" --menu "Select one of these options:" 0 0 0 1 "UEFI installation" 2 "Legacy installation" 3 "Dualboot Windows UEFI installation (recommend)" 4 "Dualboot Windows UEFI installation (not recommend)" 3>&1 1>&2 2>&3 3>&1)"

case "$choice" in
    "1") # UEFI Installation
        [ "$def_option" = "2" ] && error "No! Your machine is in Legacy mode.\nSelect Legacy installation or change your machine's boot mode to UEFI."

        if [ "$fs" = "1" ]; then
            # Create partitions
            clear
            echo "Formating hard drive..."
            parted -s $hd mklabel gpt >/dev/null 2>&1 && echo "Done!" || error "Fail to format hard drive."
            sleep 1s
            echo "Creating boot partition..."
            sgdisk $hd -n=0:0:+300M -t=0:ef00 >/dev/null 2>&1 && echo "Done!" || error "Fail to create boot partition."
            sleep 1s
            echo "Creating swap partition..."
            sgdisk $hd -n=0:0:+${SIZE[0]}G -t=0:8200 >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            sgdisk $hd -n=0:0:+${SIZE[1]}G >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s
            echo "Creating home partition..."
            sgdisk $hd -n=0:0:0 >/dev/null 2>&1 && echo "Done!" || error "Fail to create home partition."
            sleep 1s
            
            # mnt
            yes | mkfs.ext4 "${hd}3" || error "Fail to format root partition."
            mount "${hd}3" /mnt
            
            # boot
            yes | mkfs.fat -F32 "${hd}1" || error "Fail to format boot partition."
            mkdir -p /mnt/boot
            mount "${hd}1" /mnt/boot
            
            # home
            yes | mkfs.ext4 "${hd}4" || error "Fail to format home partition."
            mkdir -p /mnt/home
            mount "${hd}4" /mnt/home
            
            # swap
            mkswap "${hd}2" || error "Fail to make swap partition."
            swapon "${hd}2"
        else
            # Create partitions
            clear
            echo "Formating hard drive..."
            parted -s $hd mklabel gpt >/dev/null 2>&1 && echo "Done!" || error "Fail to format hard drive."
            sleep 1s
            echo "Creating boot partition..."
            sgdisk $hd -n=0:0:+300M -t=0:ef00 >/dev/null 2>&1 && echo "Done!" || error "Fail to create boot partition."
            sleep 1s
            echo "Creating swap partition..."
            sgdisk $hd -n=0:0:+${SIZE[0]}G -t=0:8200 >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            sgdisk $hd -n=0:0:0 >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s
            
            # mnt
            yes | mkfs.btrfs -f "${hd}3" || error "Fail to format root partition."
            mount "${hd}3" /mnt
            btrfs su cr /mnt/@
            btrfs su cr /mnt/@home
            btrfs su cr /mnt/@root
            btrfs su cr /mnt/@var
            btrfs su cr /mnt/@opt
            btrfs su cr /mnt/@tmp
            btrfs su cr /mnt/@srv
            btrfs su cr /mnt/@local
            btrfs su cr /mnt/@snapshots

            umount /mnt
            mount -o noatime,compress=lzo,space_cache,subvol=@ "${hd}3" /mnt
            mkdir -p /mnt/{home,root,var,opt,tmp,srv,usr/"local",.snapshots,}
            mount -o noatime,compress=lzo,space_cache,subvol=@home "${hd}3" /mnt/home
            mount -o noatime,compress=lzo,space_cache,subvol=@root "${hd}3" /mnt/root
            mount -o nodatacow,subvol=@var "${hd}3" /mnt/var
            mount -o noatime,compress=lzo,space_cache,subvol=@opt "${hd}3" /mnt/opt
            mount -o noatime,compress=lzo,space_cache,subvol=@tmp "${hd}3" /mnt/tmp
            mount -o noatime,compress=lzo,space_cache,subvol=@srv "${hd}3" /mnt/srv
            mount -o noatime,compress=lzo,space_cache,subvol=@local "${hd}3" /mnt/usr/local
            mount -o noatime,compress=lzo,space_cache,subvol=@snapshots "${hd}3" /mnt/.snapshots
            
            # boot
            yes | mkfs.fat -F32 "${hd}1" || error "Fail to format boot partition."
            mkdir -p /mnt/boot
            mount "${hd}1" /mnt/boot
            
            # swap
            mkswap "${hd}2" || error "Fail to make swap partition."
            swapon "${hd}2"
        fi

        ;;
    "2") # Legacy Installation
        [ "$def_option" = "1" ] && error "No! Your machine is in UEFI mode.\nSelect UEFI installation or change your machine's boot mode to Legacy."

        if [ "$fs" = "1" ]; then
            # Create partitions
            clear
            echo "Formating hard drive..."
            parted -s $hd mklabel msdos >/dev/null 2>&1 && echo "Done!" || error "Fail to format hard drive."
            sleep 1s
            echo "Creating boot partition..."
            echo -e "n\np\n\n\n+300M\na\nw" | fdisk $hd >/dev/null 2>&1 && echo "Done!" || error "Fail to create boot partition."
            sleep 1s
            echo "Creating swap partition..."
            echo -e "n\np\n\n\n+${SIZE[0]}G\nt\n\n82\nw" | fdisk $hd >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            echo -e "n\np\n\n\n+${SIZE[1]}G\nw" | fdisk $hd >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s
            echo "Creating home partition..."
            echo -e "n\np\n\n\n\nw" | fdisk $hd >/dev/null 2>&1 && echo "Done!" || error "Fail to create home partition."
            sleep 1s

            # mnt
            yes | mkfs.ext4 "${hd}3" || error "Fail to format root partition."
            mount "${hd}3" /mnt

            # boot
            yes | mkfs.ext4 "${hd}1" || error "Fail to format boot partition."
            mkdir -p /mnt/boot
            mount "${hd}1" /mnt/boot

            # home
            yes | mkfs.ext4 "${hd}4" || error "Fail to format home partition."
            mkdir -p /mnt/home
            mount "${hd}4" /mnt/home

            # swap
            mkswap "${hd}2" || error "Fail to make swap partition."
            swapon "${hd}2"
        else
            # Create partitions
            clear
            echo "Formating hard drive..."
            parted -s $hd mklabel msdos >/dev/null 2>&1 && echo "Done!" || error "Fail to format hard drive."
            sleep 1s
            echo "Creating boot partition..."
            echo -e "n\np\n\n\n+300M\na\nw" | fdisk $hd >/dev/null 2>&1 && echo "Done!" || error "Fail to create boot partition."
            sleep 1s
            echo "Creating swap partition..."
            echo -e "n\np\n\n\n+${SIZE[0]}G\nt\n\n82\nw" | fdisk $hd >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            echo -e "n\np\n\n\n\nw" | fdisk $hd >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s

            # mnt
            yes | mkfs.btrfs -f "${hd}3" || error "Fail to format root partition."
            mount "${hd}3" /mnt
            btrfs su cr /mnt/@
            btrfs su cr /mnt/@home
            btrfs su cr /mnt/@root
            btrfs su cr /mnt/@var
            btrfs su cr /mnt/@opt
            btrfs su cr /mnt/@tmp
            btrfs su cr /mnt/@srv
            btrfs su cr /mnt/@local
            btrfs su cr /mnt/@snapshots

            umount /mnt
            mount -o noatime,compress=lzo,space_cache,subvol=@ "${hd}3" /mnt
            mkdir -p /mnt/{home,root,var,opt,tmp,srv,usr/"local",.snapshots,}
            mount -o noatime,compress=lzo,space_cache,subvol=@home "${hd}3" /mnt/home
            mount -o noatime,compress=lzo,space_cache,subvol=@root "${hd}3" /mnt/root
            mount -o nodatacow,subvol=@var "${hd}3" /mnt/var
            mount -o noatime,compress=lzo,space_cache,subvol=@opt "${hd}3" /mnt/opt
            mount -o noatime,compress=lzo,space_cache,subvol=@tmp "${hd}3" /mnt/tmp
            mount -o noatime,compress=lzo,space_cache,subvol=@srv "${hd}3" /mnt/srv
            mount -o noatime,compress=lzo,space_cache,subvol=@local "${hd}3" /mnt/usr/local
            mount -o noatime,compress=lzo,space_cache,subvol=@snapshots "${hd}3" /mnt/.snapshots

            # boot
            yes | mkfs.ext4 "${hd}1" || error "Fail to format boot partition."
            mkdir -p /mnt/boot
            mount "${hd}1" /mnt/boot

            # swap
            mkswap "${hd}2" || error "Fail to make swap partition."
            swapon "${hd}2"
        fi

        ;;
    "3") # Dualboot with Windows (recommend) - UEFI
        [ -d /sys/firmware/efi ] || error "No! Your machine is in Legacy mode.\nSelect Legacy installation or change your machine's boot mode to UEFI."
        [ "$def_option" != "3" ] && error "Your machine doesn't have Windows' partitions."

        winefi="$(fdisk -l | grep "EFI System" | cut -d' ' -f1)"

        if [ "$fs" = "1" ]; then
            # Create partitions
            clear
            echo "Creating boot partition..."
            sgdisk $hd -n=0:0:+300M -t=0:ef00 >/dev/null 2>&1 && echo "Done!" || error "Fail to create boot partition."
            sleep 1s
            echo "Creating swap partition..."
            sgdisk $hd -n=0:0:+${SIZE[0]}G -t=0:8200 >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            sgdisk $hd -n=0:0:+${SIZE[1]}G >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s
            echo "Creating home partition..."
            sgdisk $hd -n=0:0:0 >/dev/null 2>&1 && echo "Done!" || error "Fail to create home partition."
            sleep 1s

            mntpart="$(fdisk -l | grep "Linux filesystem" | cut -d' ' -f1 | head -1)"
            bootpart="$(fdisk -l | grep "300M EFI" | cut -d' ' -f1)"
            homepart="$(fdisk -l | grep "Linux filesystem" | cut -d' ' -f1 | tail -1)"
            swappart="$(fdisk -l | grep "Linux swap" | cut -d' ' -f1)"

            # mnt
            yes | mkfs.ext4 "$mntpart" || error "Fail to format root partition."
            mount "$mntpart" /mnt
            sleep 1s

            # boot
            yes | mkfs.fat -F32 "$bootpart" || error "Fail to format boot partition."
            mkdir -p /mnt/boot
            mount "$bootpart" /mnt/boot
            sleep 1s

            # home
            yes | mkfs.ext4 "$homepart" || error "Fail to format home partition."
            mkdir -p /mnt/home
            mount "$homepart" /mnt/home
            sleep 1s

            # swap
            mkswap "$swappart" || error "Fail to make swap partition."
            swapon "$swappart"
            sleep 1s

            # Windows
            mkdir -p /mnt/winefi
            mount "$winefi" /mnt/winefi
        else
            # Create partitions
            clear
            echo "Creating boot partition..."
            sgdisk $hd -n=0:0:+300M -t=0:ef00 >/dev/null 2>&1 && echo "Done!" || error "Fail to create boot partition."
            sleep 1s
            echo "Creating swap partition..."
            sgdisk $hd -n=0:0:+${SIZE[0]}G -t=0:8200 >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            sgdisk $hd -n=0:0:0 >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s

            mntpart="$(fdisk -l | grep "Linux filesystem" | cut -d' ' -f1 | head -1)"
            bootpart="$(fdisk -l | grep "300M EFI" | cut -d' ' -f1)"
            swappart="$(fdisk -l | grep "Linux swap" | cut -d' ' -f1)"

            # mnt
            yes | mkfs.btrfs -f "$mntpart" || error "Fail to format root partition."
            mount "$mntpart" /mnt
            btrfs su cr /mnt/@
            btrfs su cr /mnt/@home
            btrfs su cr /mnt/@root
            btrfs su cr /mnt/@var
            btrfs su cr /mnt/@opt
            btrfs su cr /mnt/@tmp
            btrfs su cr /mnt/@srv
            btrfs su cr /mnt/@local
            btrfs su cr /mnt/@snapshots

            umount /mnt
            mount -o noatime,compress=lzo,space_cache,subvol=@ "$mntpart" /mnt
            mkdir -p /mnt/{home,root,var,opt,tmp,srv,usr/"local",.snapshots,}
            mount -o noatime,compress=lzo,space_cache,subvol=@home "$mntpart" /mnt/home
            mount -o noatime,compress=lzo,space_cache,subvol=@root "$mntpart" /mnt/root
            mount -o nodatacow,subvol=@var "$mntpart" /mnt/var
            mount -o noatime,compress=lzo,space_cache,subvol=@opt "$mntpart" /mnt/opt
            mount -o noatime,compress=lzo,space_cache,subvol=@tmp "$mntpart" /mnt/tmp
            mount -o noatime,compress=lzo,space_cache,subvol=@srv "$mntpart" /mnt/srv
            mount -o noatime,compress=lzo,space_cache,subvol=@local "$mntpart" /mnt/usr/local
            mount -o noatime,compress=lzo,space_cache,subvol=@snapshots "$mntpart" /mnt/.snapshots
            sleep 1s

            # boot
            yes | mkfs.fat -F32 "$bootpart" || error "Fail to format boot partition."
            mkdir -p /mnt/boot
            mount "$bootpart" /mnt/boot
            sleep 1s

            # swap
            mkswap "$swappart" || error "Fail to make swap partition."
            swapon "$swappart"
            sleep 1s

            # Windows
            mkdir -p /mnt/winefi
            mount "$winefi" /mnt/winefi
        fi

        ;;
    "4") # Dualboot with Windows (not recommend) - UEFI
        [ -d /sys/firmware/efi ] || error "No! Your machine is in Legacy mode.\nSelect Legacy installation or change your machine's boot mode to UEFI."
        [ "$def_option" != "3" ] && error "Your machine doesn't have Windows' partitions."

        winefi="$(fdisk -l | grep "EFI System" | cut -d' ' -f1)"

        if [ "$fs" = "1" ]; then
            # Create partitions
            clear
            echo "Creating swap partition..."
            sgdisk $hd -n=0:0:+${SIZE[0]}G -t=0:8200 >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            sgdisk $hd -n=0:0:+${SIZE[1]}G >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s
            echo "Creating home partition..."
            sgdisk $hd -n=0:0:0 >/dev/null 2>&1 && echo "Done!" || error "Fail to create home partition."
            sleep 1s

            mntpart="$(fdisk -l | grep "Linux filesystem" | cut -d' ' -f1 | head -1)"
            homepart="$(fdisk -l | grep "Linux filesystem" | cut -d' ' -f1 | tail -1)"
            swappart="$(fdisk -l | grep "Linux swap" | cut -d' ' -f1)"

            # mnt
            yes | mkfs.ext4 "$mntpart" || error "Fail to format root partition."
            mount "$mntpart" /mnt
            sleep 1s

            # boot
            mkdir -p /mnt/boot
            mount $winefi /mnt/boot

            # home
            yes | mkfs.ext4 "$homepart" || error "Fail to format home partition."
            mkdir -p /mnt/home
            mount "$homepart" /mnt/home
            sleep 1s

            # swap
            mkswap "$swappart" || error "Fail to make swap partition."
            swapon "$swappart"
            sleep 1s
        else
            # Create partitions
            clear
            echo "Creating swap partition..."
            sgdisk $hd -n=0:0:+${SIZE[0]}G -t=0:8200 >/dev/null 2>&1 && echo "Done!" || error "Fail to create swap partition."
            sleep 1s
            echo "Creating root partition..."
            sgdisk $hd -n=0:0:0 >/dev/null 2>&1 && echo "Done!" || error "Fail to create root partition."
            sleep 1s

            mntpart="$(fdisk -l | grep "Linux filesystem" | cut -d' ' -f1 | head -1)"
            swappart="$(fdisk -l | grep "Linux swap" | cut -d' ' -f1)"

            # mnt
            yes | mkfs.btrfs -f "$mntpart" || error "Fail to format root partition."
            mount "$mntpart" /mnt
            btrfs su cr /mnt/@
            btrfs su cr /mnt/@home
            btrfs su cr /mnt/@root
            btrfs su cr /mnt/@var
            btrfs su cr /mnt/@opt
            btrfs su cr /mnt/@tmp
            btrfs su cr /mnt/@srv
            btrfs su cr /mnt/@local
            btrfs su cr /mnt/@snapshots

            umount /mnt
            mount -o noatime,compress=lzo,space_cache,subvol=@ "$mntpart" /mnt
            mkdir -p /mnt/{home,root,var,opt,tmp,srv,usr/"local",.snapshots,}
            mount -o noatime,compress=lzo,space_cache,subvol=@home "$mntpart" /mnt/home
            mount -o noatime,compress=lzo,space_cache,subvol=@root "$mntpart" /mnt/root
            mount -o nodatacow,subvol=@var "$mntpart" /mnt/var
            mount -o noatime,compress=lzo,space_cache,subvol=@opt "$mntpart" /mnt/opt
            mount -o noatime,compress=lzo,space_cache,subvol=@tmp "$mntpart" /mnt/tmp
            mount -o noatime,compress=lzo,space_cache,subvol=@srv "$mntpart" /mnt/srv
            mount -o noatime,compress=lzo,space_cache,subvol=@local "$mntpart" /mnt/usr/local
            mount -o noatime,compress=lzo,space_cache,subvol=@snapshots "$mntpart" /mnt/.snapshots
            sleep 1s

            # boot
            mkdir -p /mnt/boot
            mount $winefi /mnt/boot

            # swap
            mkswap "$swappart" || error "Fail to make swap partition."
            swapon "$swappart"
            sleep 1s
        fi

        ;;
esac

# Check partitions and mount points
result="$(lsblk -plo NAME,SIZE,MOUNTPOINT -e 7,11)"
dialog --title "SALI Installation" --no-collapse --yesno "$result" 0 0 || exit 1
clear

# Get kernel
case $ker_choice in
    1)
        kernel=linux
        ker_header=linux-headers
        ;;
    2)
        kernel=linux-lts
        ker_header=linux-lts-headers
        ;;
esac

# Install some basic packages
pacman -Sy wget archlinux-keyring --noconfirm
pacstrap /mnt base base-devel $kernel linux-firmware $ker_header wget "$([ "$fs" = "2" ] && echo btrfs-progs || echo grub)"

# Modify fstab file
genfstab -U /mnt >> /mnt/etc/fstab

mv *.sali /mnt
mv hostname /mnt/etc/hostname

# chroot
read -rd '' sali_chroot <<'EOF'
# Download dependency
pacman -S dialog --noconfirm --needed

# Set root password
rootpw="$(cat rootpw.sali)"
echo "root:$rootpw" | chpasswd
unset rootpw

# Add user and set its password
username="$(cat username.sali)"
useradd -m -G wheel,audio,video,optical,storage,power -s /bin/bash "$username" >/dev/null 2>&1 ||
usermod -aG wheel,audio,video,optical,storage,power "$username" && mkdir -p /home/"$username" && chown "$username":"$username" /home/"$username"
userpw="$(cat userpw.sali)"
echo "$username:$userpw" | chpasswd
unset userpw
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' /etc/sudoers

# Set time zone
tzone=$(cat timezone.sali)
ln -sf /usr/share/zoneinfo/$tzone /etc/localtime
hwclock --systohc

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

pcname="$(cat /etc/hostname)"
echo -e "127.0.0.1\tlocalhost
::1\t\t\tlocalhost
127.0.1.1\t$pcname.localdomain\t$pcname" >> /etc/hosts

# Network manager
pacman -S networkmanager network-manager-applet --noconfirm --needed
systemctl enable NetworkManager

# Grub
option="$(cat option.sali)"
[ "$option" = "1" ] &&
pacman --noconfirm --needed -S grub efibootmgr &&
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB  &&
grub-mkconfig -o /boot/grub/grub.cfg

[ "$option" = "2" ] &&
pacman --noconfirm --needed -S grub &&
grub-install "$(cat hd.sali)" &&
grub-mkconfig -o /boot/grub/grub.cfg

[ "$option" = "3" ] &&
pacman --noconfirm --needed -S grub efibootmgr os-prober &&
os-prober &&
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB  &&
grub-mkconfig -o /boot/grub/grub.cfg

# Delete all junk
rm *.sali
EOF

printf '%s\n' "$sali_chroot" > /mnt/root/chroot.sh
chmod +x /mnt/root/chroot.sh

arch-chroot /mnt /root/chroot.sh
rm /mnt/root/chroot.sh

# Last messages
dialog --defaultno --title "SALI Installation" --yesno "Reboot computer?"  5 30 && { umount -R /mnt; reboot; }
dialog --defaultno --title "SALI Installation" --yesno "Return to chroot environment?"  6 30 && arch-chroot /mnt
clear

exit 0
