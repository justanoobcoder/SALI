# Syaoran's Arch Linux Installer (SALI)

## What is SALI?

Syaoran's Arch Linux Installer (SALI) is my personal script to install Arch linux. This script can install Arch single boot (UEFI or Legacy) and dualboot with Windows (UEFI). There are two options for dualbooting with Windows: recommend one and not recommend one.

SALI can install [SARS](https://gitlab.com/justanoobcoder/SARS) to setup a full-featured Arch linux desktop after its installation but you can choose No if you don't want to.

## Installation

First, boot with the last Arch Linux image with a bootable device.

Make sure you have internet. Then run the following:

    bash <(curl -Ls bit.ly/sali-sh)

You need to input some information at the beginning and then the script will automatically install Arch linux for you.

## Notes

This script will create 4 partitions: one for boot (default 300MiB), one for swap, one for root and the rest for home.

If you install Arch along side with Windows, you need to create an empty partition first (it's shown Unallocated when you're in Disk Management on Windows). The size of that empty partition should be at least 50GiB. The script will install Arch linux on that partition. When the installation (the recommend one) is done and you restart your machine, your machine will probably boot into Windows. If that happens, restart your machine, go to boot menu, create a new boot option, name it GRUB and try to find its file name - something like `grubx64.efi`, save it. Finally, move GRUB boot option above Windows boot option. And restart the machine. It should be booted to Grub where you can choose which operating system you want to get into. If you're not familiar with what I've just said, just choose the other dualboot Windows option (not recommend one) but your Windows and Arch might mess up with each other when you update Windows. Remember that.

SALI doesn't work if you want to install Arch linux and Windows on different hard drives.
